package com.tatahealth.ConsumerAppointment.Billing;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.json.JSONObject;
import org.testng.annotations.Test;

import com.tatahealth.API.Billing.AppointmentSlotSelection;
import com.tatahealth.API.Billing.FitCoins;
import com.tatahealth.API.Billing.Login;
import com.tatahealth.API.Billing.PaymentDetails;
import com.tatahealth.API.Core.CCAvenueDetails;
import com.tatahealth.API.Core.CommonFunctions;
import com.tatahealth.API.Core.Encryption;
import com.tatahealth.API.libraries.ExcelIntegration;

public class PaymentModeCases extends Login {

	public List<NameValuePair> headers = null;
	PaymentDetails p = null;
	private Integer remainingAmount = 0;
	private Integer discountAmount = 0;

	public void bookAppointment(String mop) throws Exception {
		System.out.println("Book Appointment : " + mop);

		String paid = null;
		CommonFunctions func = new CommonFunctions();
		AppointmentSlotSelection slotSelection = new AppointmentSlotSelection();
		slotSelection.searchDoctor();
		slotSelection.getDoctorInfo();
		// slotSelection.checkSelectedSlotisAvailable();

		JSONObject param = new JSONObject();
		param.put("appointmentBookedDate",appointmentBookedDate);
		param.put("clinicUserId", doctorId);
		param.put("appointmentType", appointmentType);
		param.put("consumerUserId", consumerUserId);
		param.put("consumerLatitude", consumerLatitude);
		param.put("consumerLongitude", consumerLongitude);
		param.put("dateSlotId", dateSlotId);
		param.put("endTime", bookedSlot.getString("slotEndTime"));
		param.put("locationId", bookedSlot.getInt("locationId"));
		param.put("organizationId", orgId);
		param.put("siteId", bookedSlot.getInt("siteId"));
		param.put("slotId", bookedSlot.getInt("clinicUserSlotId"));
		param.put("startTime", bookedSlot.getString("slotStartTime"));

		if (mop.equalsIgnoreCase("Pay At Clinic")) {
			System.out.println("inside pay at clinic");
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 0);
			param.put("purchasePackagesId", 0);
			param.put("payAtClinicAmt", appointmentAmount);
			paid = appointmentAmount + "/PC";

		} else if (mop.equalsIgnoreCase("Package")) {
			System.out.println("Inside Package");
			slotSelection.packageAndRewardDetails("Package", "Full Payment");
			param.put("carePackagedId", carePackageId);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 2);
			param.put("purchasePackagesId", purchasePackageId);
			paid = appointmentAmount + "/P";

		} else if (mop.equalsIgnoreCase("Coupon")) {
			System.out.println("inside Coupon");
			slotSelection.packageAndRewardDetails("Coupon", "THREE");
			slotSelection.redemptionCoupon();
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 2);
			param.put("purchasePackagesId", 0);
			param.put("redemptionId", redemptionId);
			param.put("coupon", couponpackageObj);
			paid = appointmentAmount + "/C";

		} else if (mop.equalsIgnoreCase("Online Payment")) {
			System.out.println("inside online payment");
			CCAvenueDetails avenue = new CCAvenueDetails();
			avenue.getCCAvenueResponse("appointment");
			avenue.saveCCAvenueResponse(appointmentAmount.toString());
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", orderId);
			param.put("payementTypeFlag", 1);
			param.put("purchasePackagesId", 0);
			paid = appointmentAmount + "/O";

		} else if (mop.equalsIgnoreCase("Fit Coin")) {
			System.out.println("inside fit coin");
			slotSelection.packageAndRewardDetails("Rewards", "Full");
			if (rewardBalance >= appointmentAmount) {
				rewardBalance = appointmentAmount;
			}
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", rewardBalance);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 2);
			param.put("purchasePackagesId", 0);
			paid = appointmentAmount + "/F";

		} else if (mop.equalsIgnoreCase("Coupon + Online Payment")) {
			System.out.println("inside coupon  + op");
			slotSelection.packageAndRewardDetails("Coupon", "LABTESTONE");
			slotSelection.redemptionCoupon();
			discountAmount = couponpackageObj.getInt("discountAmount");
			remainingAmount = appointmentAmount - discountAmount;
			CCAvenueDetails avenue = new CCAvenueDetails();
			avenue.getCCAvenueResponse("appointment");
			avenue.saveCCAvenueResponse(remainingAmount.toString());
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", orderId);
			param.put("payementTypeFlag", 1);
			param.put("purchasePackagesId", 0);
			param.put("redemptionId", redemptionId);
			param.put("coupon", couponpackageObj);
			paid = discountAmount + "/C+" + remainingAmount + "/O";

		} else if (mop.equalsIgnoreCase("Package + Online Payment")) {
			System.out.println("inside p + op");
			slotSelection.packageAndRewardDetails("Package", "Partial Payment");
			remainingAmount = slotSelection.getPackageDiscount();
			discountAmount = appointmentAmount - remainingAmount;
			// TO-DO remove debug statement
			System.out.println("starting cc avenue save");
			CCAvenueDetails avenue = new CCAvenueDetails();
			avenue.getCCAvenueResponse("appointment");
			avenue.saveCCAvenueResponse(remainingAmount.toString());
			// TO-DO remove debug statement
			System.out.println("Done with cc avenue");
			param.put("carePackagedId", carePackageId);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", orderId);
			param.put("payementTypeFlag", 1);
			param.put("purchasePackagesId", purchasePackageId);
			paid = discountAmount + "/P+" + remainingAmount + "/O";

		} else if (mop.equalsIgnoreCase("Coupon + Pay At Clinic")) {
			System.out.println("inside c+pc");
			slotSelection.packageAndRewardDetails("Coupon", "LABTESTONE");
			slotSelection.redemptionCoupon();
			discountAmount = couponpackageObj.getInt("discountAmount");
			remainingAmount = appointmentAmount - discountAmount;
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 0);
			param.put("purchasePackagesId", 0);
			param.put("payAtClinicAmt", remainingAmount);
			param.put("redemptionId", redemptionId);
			param.put("coupon", couponpackageObj);
			paid = discountAmount + "/C+" + remainingAmount + "/PC";

		} else if (mop.equalsIgnoreCase("Coupon + Fit Coin")) {
			System.out.println("inside c+f");
			slotSelection.packageAndRewardDetails("Coupon", "LABTESTONE");
			slotSelection.redemptionCoupon();
			discountAmount = couponpackageObj.getInt("discountAmount");
			remainingAmount = appointmentAmount - discountAmount;
			slotSelection.packageAndRewardDetails("Rewards", "Full");
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", remainingAmount);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 2);
			param.put("purchasePackagesId", 0);
			param.put("redemptionId", redemptionId);
			param.put("coupon", couponpackageObj);
			paid = discountAmount + "/C+" + remainingAmount + "/F";

		} else if (mop.equalsIgnoreCase("Coupon + Fit Coin + Online Payment")) {
			System.out.println("inside c+f+op");
			// System.out.println("appointment amount : "+ appointmentAmount);
			slotSelection.packageAndRewardDetails("Coupon", "PHARMA150");
			slotSelection.redemptionCoupon();
			discountAmount = couponpackageObj.getInt("discountAmount");
			remainingAmount = appointmentAmount - discountAmount;
			slotSelection.packageAndRewardDetails("Rewards", "Partial");
			if (rewardBalance >= remainingAmount) {
				rewardBalance = 50;
				remainingAmount = remainingAmount - rewardBalance;
			} else {
				remainingAmount = remainingAmount - rewardBalance;
			}
			System.out.println("coupon dis : " + discountAmount + " reward balance : " + rewardBalance + " online : "
					+ remainingAmount);
			CCAvenueDetails avenue = new CCAvenueDetails();
			avenue.getCCAvenueResponse("appointment");
			avenue.saveCCAvenueResponse(remainingAmount.toString());
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", rewardBalance);
			param.put("paymentOrderId", orderId);
			param.put("payementTypeFlag", 1);
			param.put("purchasePackagesId", 0);
			param.put("redemptionId", redemptionId);
			param.put("coupon", couponpackageObj);
			paid = discountAmount + "/C+" + rewardBalance + "/F+" + remainingAmount + "/O";

		} else if (mop.equalsIgnoreCase("Package + Pay At Clinic")) {
			System.out.println("inside p+pc");
			slotSelection.packageAndRewardDetails("Package", "Partial Payment");
			remainingAmount = slotSelection.getPackageDiscount();
			discountAmount = appointmentAmount - remainingAmount;
			param.put("carePackagedId", carePackageId);
			param.put("consumableRewardAmt", 0);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 0);
			param.put("purchasePackagesId", purchasePackageId);
			param.put("payAtClinicAmt", remainingAmount);
			paid = discountAmount + "/P+" + remainingAmount + "/PC";

		} else if (mop.equalsIgnoreCase("Fit Coin + Pay At Clinic")) {
			System.out.println("inside f+pc");
			slotSelection.packageAndRewardDetails("Rewards", "Partial");

			if (rewardBalance >= appointmentAmount) {
				rewardBalance = 50;
				remainingAmount = appointmentAmount - rewardBalance;
			} else {
				remainingAmount = appointmentAmount - rewardBalance;
			}
			System.out.println("fit coin : " + remainingAmount + " reward balance : " + rewardBalance);
			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", rewardBalance);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 0);
			param.put("purchasePackagesId", purchasePackageId);
			param.put("payAtClinicAmt", remainingAmount);
			paid = rewardBalance + "/F+" + remainingAmount + "/PC";

		} else if (mop.equalsIgnoreCase("Fit Coin + Online Payment")) {
			System.out.println("inside F+op");
			slotSelection.packageAndRewardDetails("Rewards", "Partial");
			if (rewardBalance >= appointmentAmount) {
				rewardBalance = 50;
				remainingAmount = appointmentAmount - rewardBalance;
			} else {
				remainingAmount = appointmentAmount - rewardBalance;
			}
			CCAvenueDetails avenue = new CCAvenueDetails();
			avenue.getCCAvenueResponse("appointment");
			avenue.saveCCAvenueResponse(remainingAmount.toString());

			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", rewardBalance);
			param.put("paymentOrderId", orderId);
			param.put("payementTypeFlag", 1);
			param.put("purchasePackagesId", 0);
			paid = rewardBalance + "/F+" + remainingAmount + "/O";

		} else if (mop.equalsIgnoreCase("Coupon + Fit Coin + Pay At Clinic")) {
			System.out.println("inside c+f+pc");
			slotSelection.packageAndRewardDetails("Coupon", "PHARMA150");
			slotSelection.redemptionCoupon();
			discountAmount = couponpackageObj.getInt("discountAmount");
			remainingAmount = appointmentAmount - discountAmount;
			slotSelection.packageAndRewardDetails("Rewards", "Partial");
			if (rewardBalance >= remainingAmount) {
				rewardBalance = 50;
				remainingAmount = remainingAmount - rewardBalance;
			} else {
				remainingAmount = remainingAmount - rewardBalance;
			}

			param.put("carePackagedId", 0);
			param.put("consumableRewardAmt", rewardBalance);
			param.put("paymentOrderId", 0);
			param.put("payementTypeFlag", 0);
			param.put("purchasePackagesId", 0);
			param.put("payAtClinicAmt", remainingAmount);
			param.put("redemptionId", redemptionId);
			param.put("coupon", couponpackageObj);
			paid = discountAmount + "/C+" + rewardBalance + "/F+" + remainingAmount + "/PC";
		}

		url = baseurl + "/appointment/api/v1/patient/book-appointment";
		headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		headers.add(new BasicNameValuePair("misc", userDOB));
		headers.add(new BasicNameValuePair("visitorId", "2dfc78d3-11a0-49de-b842-092c756c68dd"));
		headers.add(new BasicNameValuePair("health_vault_token",""));
		headers.add(new BasicNameValuePair("platformId","3"));
		headers.add(new BasicNameValuePair("Content-Type","text/plain"));
		String text = param.toString();
		Encryption enc = new Encryption();
		String encKey = "TATADHP-" + userDOB;
		// System.out.println("encryKey : "+encKey);
		String encText = enc.encrypt(text, encKey);
		func.log("Mode of payment", mop);
		func.log("headers", headers);
		func.log("Params", param);
		// System.out.println("headers : "+headers + " "+url);

		// System.out.println("encryption request : "+encText);
		// TO-DO remove debug statement
		System.out.println("Booking appointemnt...");

		JSONObject responseObj = func.PostHeaderWithTextRequest(url, encText, headers);
		func.log("Response:", responseObj);
		message = responseObj.getJSONObject("status").getString("message");
		System.out.println("Appointment booking message : " + message);
		appointmentId = responseObj.getJSONObject("responseData").getInt("appointmentId");
		p = new PaymentDetails();
		p.setModeOfPayment(mop);
		p.setSlotId(String.valueOf(bookedSlot.getInt("clinicUserSlotId")));
		p.setAmount(appointmentAmount);
		p.setServiceDiscount(0);
		p.setAdditionalServiceAmount(0);
		p.setAdditionalDiscount(0);
		p.setPaid(paid);
		p.setAppointmentName(consumerName);
		p.setBillId(0);
		paymentObjectMapping.put(mop, p);

	}

}
