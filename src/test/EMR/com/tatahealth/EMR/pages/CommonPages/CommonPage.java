package com.tatahealth.EMR.pages.CommonPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class CommonPage {
	
	
	public WebElement getPoweredByLink(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#powredByLink > img", driver, logger);
		return element;
	}
	
	
	public WebElement getToastMessage(WebDriver driver,ExtentTest logger,int time) {
		WebElement element = Web_GeneralFunctions.findElementbySelector(".toast-message:nth-child(3)", driver, logger);
		return element;
	}
	
	
	public WebElement getPatientRegistrationToast(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector(".toast-message:nth-child(3)", driver, logger);
		return element;
	}
	
	
	
	public WebElement getConsultationAlert(WebDriver driver,ExtentTest logger,int time) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#pendingAppointmentModel > div > div > div.modal-body.graybg > div:nth-child(10) > div.col-md-12 > a:nth-child(1)", driver, logger,time);
		return element;
	}
	
	
	public WebElement getConsultationAlert(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#pendingAppointmentModel > div > div > div.modal-body.graybg > div:nth-child(10) > div.col-md-12 > a:nth-child(1)", driver, logger);
		return element;
	}
}