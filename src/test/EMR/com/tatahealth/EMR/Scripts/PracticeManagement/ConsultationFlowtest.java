
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.ReferralPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.PracticeManagement.ConsultationFlowPages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class ConsultationFlowtest {
	AppointmentsPage ap = new AppointmentsPage();
	SymptomPage sp = new SymptomPage();
	DiagnosisPage dp = new DiagnosisPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	ReferralPage rp = new ReferralPage();
	PracticeManagementTest pmt = new PracticeManagementTest();
	PracticeManagementPages pm = new PracticeManagementPages();
	VitalMasterTest vmt = new VitalMasterTest();
	ConsultationFlowPages cfw = new ConsultationFlowPages();
	public static int position = 0;
	ExtentTest logger;
	
	//initializing driver and login 
	@BeforeClass(alwaysRun=true,groups= {"Regression","Login"})
	public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR_PracticeManagement_ConsultationFlow";
		Login_Doctor.LoginTest();
		new MedicalKItTest().closePreviousConsultations();
		new PracticeManagementTest().moveToPracticeManagement();
	} 
	
	//Logging out and closing driver
	@AfterClass(groups = { "Regression","Logout"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
	}
	
	//Test Case for PM_78
	@Test(groups= {"Regression","PracticeManagement"},priority=482)
	public synchronized void validteModulesDispaly() throws Exception {
		logger = Reports.extent.createTest("EMR verify display of Consultation flow modules");
		moveToConsultationFlow();
		assertTrue(Web_GeneralFunctions.isDisplayed(cfw.getPreExaminationModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfw.getDiagnosisAndPrescriptionModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfw.getLabResultModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfw.getGlobalPrintModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfw.getCaseSheetModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(cfw.getReferralAndFollowUpParentModule(Login_Doctor.driver, logger)));
		}
	
	//Test Case for PM_80
		@Test(groups= {"Regression","PracticeManagement"},priority=483)
	public synchronized void verifyDefaultSelectionsStatus() throws InterruptedException {
		logger = Reports.extent.createTest("EMR verifying Diagnosis/Prescription and Global print are selected and disabled");
		assertTrue(Web_GeneralFunctions.isChecked(cfw.getDiagnosisCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver));
		assertTrue(Web_GeneralFunctions.isChecked(cfw.getGlobalPrintCheckbox(Login_Doctor.driver, logger), Login_Doctor.driver));
		assertTrue(!cfw.getDiagnosisCheckbox(Login_Doctor.driver, logger).isEnabled());
		assertTrue(!cfw.getGlobalPrintCheckbox(Login_Doctor.driver, logger).isEnabled());
	}
	
	//Test Case for PM_79
	@Test(groups= {"Regression","PracticeManagement"},priority=484)
	public synchronized void selectAllModuleSections() throws Exception {
		logger = Reports.extent.createTest("EMR Selecting All Options if deselected");
		
		WebElement PreExam = cfw.getPreExaminationCheckbox(Login_Doctor.driver, logger);
		WebElement ReferralAndFollowup = cfw.getReferralAndFollowupCheckbox(Login_Doctor.driver, logger);
		WebElement CaseSheet = cfw.getCaseSheetCheckbox(Login_Doctor.driver, logger);
		WebElement LabResults = cfw.getLabResultsCheckbox(Login_Doctor.driver, logger);
		WebElement Referral = cfw.getReferralCheckbox(Login_Doctor.driver, logger);
		WebElement Symptoms = cfw.getSymptomsCheckbox(Login_Doctor.driver, logger);
		WebElement Followup = cfw.getFollowupCheckbox(Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(PreExam, Login_Doctor.driver)) 
			Web_GeneralFunctions.click(PreExam, "selecting pre examination checkbox", Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(ReferralAndFollowup, Login_Doctor.driver))
			Web_GeneralFunctions.click(ReferralAndFollowup, "Selecting Referral and Followup checkbox", Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(CaseSheet, Login_Doctor.driver))
			Web_GeneralFunctions.click(CaseSheet, "Selecting CaseSheet checkbox", Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(LabResults, Login_Doctor.driver))
			Web_GeneralFunctions.click(LabResults, "Selecting Lab Results checkbox", Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(Referral, Login_Doctor.driver))
			Web_GeneralFunctions.click(Referral, "Selecting Referral checkbox", Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(Symptoms, Login_Doctor.driver))
			Web_GeneralFunctions.click(Symptoms, "Selecting Symptoms checkbox", Login_Doctor.driver, logger);
		if(!Web_GeneralFunctions.isChecked(Followup, Login_Doctor.driver))
			Web_GeneralFunctions.click(Followup, "Selecting Followup checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		saveConsultationFlow();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getMessage(Login_Doctor.driver, logger));
	
	}
	
	//Test case PM_81 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=485)
	public synchronized void moveCaseSheetToLastPOsition()throws Exception{
		
		logger = Reports.extent.createTest("EMR move case sheet to last position");
		WebElement fromElement = cfw.getCaseSheetModule(Login_Doctor.driver, logger);
		position = 5;
		WebElement toElement = cfw.getParentModule(position, Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "move to last postion", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=486)
	public synchronized void saveSuccessConsultationFlow()throws Exception{
		
		logger = Reports.extent.createTest("EMR save Consultation flow");
		saveConsultationFlow();
		String text = Web_GeneralFunctions.getText(cfw.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Configuration saved successfully"));
	}
	
	//Test Case PM_82 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=487)
	public synchronized void verifiedInConsultationPage()throws Exception{
		
		logger = Reports.extent.createTest("EMR verified in  Consultation paage");
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		String attribute = Web_GeneralFunctions.getAttribute(cfw.getExaminationPositionInConsultationPage(Login_Doctor.driver, logger), "data-input", "get position", Login_Doctor.driver, logger);
		assertTrue(attribute.trim().equalsIgnoreCase("Examination"));
		backToConsultationFlow();
	}
	
	//Test case PM_81 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=488)
	public synchronized void moveDiagnosisToFirstPosition() throws Exception {
		logger  = Reports.extent.createTest("EMR move Diagnosis to First position");
		WebElement toElement = cfw.getDiagnosisAndPrescriptionModule(Login_Doctor.driver, logger);
		position = 0;
		WebElement fromElement = cfw.getParentModule(position,Login_Doctor.driver,logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Move to first position", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		saveConsultationFlow();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getMessage(Login_Doctor.driver, logger));
		
	}
	
	//Test case PM_82 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=489)
	public synchronized void verifyDiagnosisPositionInConsultationPage() throws Exception {
		logger  = Reports.extent.createTest("EMR Verify Diagnosis position in consultation page");
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		String attribute = Web_GeneralFunctions.getAttribute(cfw.getDiagnosisPositionInConsultationPage(Login_Doctor.driver, logger), "data-input", "get position", Login_Doctor.driver, logger);
		assertTrue(attribute.equalsIgnoreCase("Prescription"));
		backToConsultationFlow();
		WebElement fromElement = cfw.getDiagnosisAndPrescriptionModule(Login_Doctor.driver, logger);
		position = 1;
		WebElement toElement = cfw.getReferralAndFollowUpParentModule(Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Move to first position", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		saveConsultationFlow();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getMessage(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=490)
	public synchronized void moveSymptomsToDiagnosisSubSection()throws Exception{
		
		logger = Reports.extent.createTest("EMR try to move symptoms to diagnosis module");
		WebElement fromElement = cfw.getSymptomModule(Login_Doctor.driver, logger);
		WebElement toElement = cfw.getDiagnosisAndPrescriptionModule(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(fromElement, "clicking on symptoms", Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Drag and drop the symptom module to diagnosis", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		saveConsultationFlow();
		String text = Web_GeneralFunctions.getText(cfw.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		moveToConsultationFlow();
		assertTrue(text.equalsIgnoreCase("Symptoms or Vitals sub section cannot be moved out of Pre-examination"));
		
	}
	
	//Test Case PM_85 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=491)
	public synchronized void moveSubSectionToGlobalPrintModule()throws Exception{
		
		logger = Reports.extent.createTest("EMR  move symptom sub section to global print module");
		WebElement fromElement =cfw.getSymptomModule(Login_Doctor.driver, logger);
		WebElement toElement = cfw.getLabResultModule(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(fromElement, "clicking on symptoms", Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Drag and drop the symptom module to Globalprint", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		saveConsultationFlow();
		String text = Web_GeneralFunctions.getText(cfw.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Sub section is not allowed in Examination, Global Print or Lab Results"));
	}
	
	//Test case PM_83 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=492)
	public synchronized void moveSubSectionToLabResultModule()throws Exception{
		
		logger = Reports.extent.createTest("EMR  move vital sub section to lab result module");
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		moveToConsultationFlow();
		WebElement fromElement = cfw.getVitalModule(Login_Doctor.driver, logger);
		WebElement toElement = cfw.getCaseSheetModule(Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Drag and drop the symptom module to Lab Results", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		saveConsultationFlow();
		String text = Web_GeneralFunctions.getText(cfw.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Sub section is not allowed in Examination, Global Print or Lab Results"));
	}
	
	
	//Test Case PM_84 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=493)
	public synchronized void moveSubSectionToCaseSheetModule()throws Exception{
		logger = Reports.extent.createTest("EMR move vital sub section to case sheet module");
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		moveToConsultationFlow();
		WebElement fromElement = cfw.getVitalsCheckbox(Login_Doctor.driver, logger);
		WebElement toElement = cfw.getCaseSheetCheckbox(Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "Drag and drop the symptom module to Casesheet Module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		saveConsultationFlow();
		String text = Web_GeneralFunctions.getText(cfw.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Sub section is not allowed in Examination, Global Print or Lab Results"));

	}
	
	//Test Case PM_82 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=494)
	public synchronized void unselectReferralAndFollowUpModule()throws Exception{
		logger = Reports.extent.createTest("EMR unselect referral and follow up module");
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		moveToConsultationFlow();
		Web_GeneralFunctions.click(cfw.getReferralAndFollowUpParentModule(Login_Doctor.driver, logger), "click referral and follow up module", Login_Doctor.driver, logger);
		saveConsultationFlow();
	}
	
	//Test Case PM_82 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=495)
	public synchronized void verifiedReferralModulePresentInConsultationPage()throws Exception{
		
		logger = Reports.extent.createTest("EMR unselect referral module and validate in consultation page");
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		assertTrue(cfw.getReferralMenuInConsultationPage(Login_Doctor.driver, logger)==null);
		backToConsultationFlow();
		unselectReferralAndFollowUpModule();
	}
	
	//Test case PM_79 ,PM_82 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=496)
	public synchronized void unselectAllParentModulesAndValidateInConsultationFlow() throws Exception {
		logger = Reports.extent.createTest("EMR Unselect All parent Elements and validate Consultation flow");
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.click(pm.getLeftMainMenu(Login_Doctor.driver, logger), "Click Left main menu ", Login_Doctor.driver, logger);
		WebElement PreExam = cfw.getPreExaminationCheckbox(Login_Doctor.driver, logger);
		WebElement ReferralAndFollowup = cfw.getReferralAndFollowupCheckbox(Login_Doctor.driver, logger);
		WebElement CaseSheet = cfw.getCaseSheetCheckbox(Login_Doctor.driver, logger);
		WebElement LabResults = cfw.getLabResultsCheckbox(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(PreExam, "selecting pre examination checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ReferralAndFollowup, "Selecting Referral and Followup checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(CaseSheet, "Selecting CaseSheet checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(LabResults, "Selecting Lab Results checkbox", Login_Doctor.driver, logger);
		saveConsultationFlow();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getMessage(Login_Doctor.driver, logger));
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		assertTrue(cfw.getReferralMenuInConsultationPage(Login_Doctor.driver, logger)==null);
		assertTrue(cfw.getLabResultsMenuInConsultationPage(Login_Doctor.driver, logger)==null);
		assertTrue(cfw.getPreExaminationMenuInConsultationPage(Login_Doctor.driver, logger)==null);
		assertTrue(cfw.getCaseSheetMenuInConsultationPage(Login_Doctor.driver, logger)==null);
		backToConsultationFlow();
		selectAllModuleSections();
		saveConsultationFlow();
		
	}
	
	public synchronized void movePreExaminationToFirstPosition() throws Exception {
		logger = Reports.extent.createTest("EMR move PreExamination to first position");
		WebElement toElement = cfw.getPreExaminationModule(Login_Doctor.driver, logger);
		position = 0;
		WebElement fromElement = cfw.getParentModule(position, Login_Doctor.driver, logger);
		Web_GeneralFunctions.dragAndDrop(fromElement, toElement, "move to first postion", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		saveConsultationFlow();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getMessage(Login_Doctor.driver, logger));
	}
	
	
	public synchronized void backToConsultationFlow()throws Exception{
		logger = Reports.extent.createTest("EMR Back to Consultation Flow from consultation page");
		pmt.moveToPracticeManagement();
		moveToConsultationFlow();
	}
	
	public synchronized void moveToConsultationFlow()throws Exception{
		logger = Reports.extent.createTest("EMR move to Consultation flow");
		Web_GeneralFunctions.click(cfw.moveToConsultationFlow(Login_Doctor.driver, logger), "Move to consultation flow", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.saveCosnultationFlow(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getGlobalPrint(Login_Doctor.driver, logger));
	}
	
	public synchronized void saveConsultationFlow() throws Exception {
		Web_GeneralFunctions.click(cfw.saveCosnultationFlow(Login_Doctor.driver, logger), "save consultation flow", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, cfw.getMessage(Login_Doctor.driver, logger));
	}
}

